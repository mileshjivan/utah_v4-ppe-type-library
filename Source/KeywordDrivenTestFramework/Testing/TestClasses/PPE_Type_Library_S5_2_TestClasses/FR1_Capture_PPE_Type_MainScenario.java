/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Type_Library_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Type_Library_V5_2_PageObjects.PPE_Type_Library_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture PPE Type v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_PPE_Type_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_PPE_Type_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_PPE_Type_Library()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_PPE_Type_Library()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured PPE Type Library record.");
    }

    public boolean Navigate_To_PPE_Type_Library() {
//        pause(5000);
//        //Navigate to Environmental Health & Safety
//        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.navigate_EHS())) {
//            error = "Failed to wait for 'Environmental, Health & Safety' button.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.navigate_EHS())) {
//            error = "Failed to click on 'Environmental, Health & Safety' button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Operate Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.OperateMaintenance())) {
            error = "Failed to wait for 'Operate Maintenance' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.OperateMaintenance())) {
            error = "Failed to click on 'Operate Maintenance' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Operate Maintenance' button.");

        //Navigate to PPE Type Library
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.PPE_Type_Library_Module())) {
            error = "Failed to wait for 'PPE Type Library' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.PPE_Type_Library_Module())) {
            error = "Failed to click on 'PPE Type Library' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'PPE Type Library' Module.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.PPE_Type_Library_AddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.PPE_Type_Library_AddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_PPE_Type_Library() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.businessUnit_dropdown())) {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.businessUnit_dropdown())) {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.businessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to wait for Business Unit: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.businessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.businessUnit_Option(getData("Business Unit 2")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.businessUnit_Checkbox(getData("Business Unit 3")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 3");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.businessUnit_dropdown())) {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));
 
        //PPE Type
        if (!SeleniumDriverInstance.enterTextByXpath(PPE_Type_Library_PageObject.PPE_Type(), getData("PPE Type"))) {
            error = "Failed to '" + getData("PPE Type") + "' into PPE Type field";
            return false;
        }
        narrator.stepPassedWithScreenShot("PPE Type : '" + getData("PPE Type") + "'.");
        
        //Details
        if (!SeleniumDriverInstance.enterTextByXpath(PPE_Type_Library_PageObject.Details(), getData("Details"))) {
            error = "Failed to '" + getData("Details") + "' into Details field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Details : '" + getData("Details") + "'.");
        
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        pause(10000);
        
//        //Saving mask
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(PPE_Type_Library_PageObject.saveWait2())) {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }
//
//        //Validate if the record has been saved or not.
//        if (!SeleniumDriverInstance.waitForElementsByXpath(PPE_Type_Library_PageObject.validateSave())){
//            error = "Failed to wait for Save validation.";
//            return false;
//        }
//
//        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(PPE_Type_Library_PageObject.validateSave());
//
//        if (!SaveFloat.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Failed to save record.");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
}
