/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.PPE_Type_Library_V5_2_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author SKhumalo
 */
public class PPE_Type_Library_PageObject extends BaseClass {

    public static String BusinessUnitHeader() {
        return "//div[text()='Business unit']";
    }

    public static String PPE() {
        return "(//div[@id='control_560F3A65-F064-4CE4-AE2F-3DD19F66BA14']//input)[1]";
    }
    public static String PPE_Items_AddButton() {
        return "//div[@id='control_2D0304BD-231F-4256-8586-4E5135BFC65F']//div[text()='Add']";
    }
    public static String PPE_Type_Library() {
        return "//div[text()='PPE Type Library']";
    }
    public static String URL() {
        return "(//div[@id='control_25F54274-4A8D-4ED7-B2DB-201C317C627B']//input)[1]";
    }
    public static String searchBtn() {
        return "//div[@id='searchOptions']//div[text()='Search']";
    }
    public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
    public static String reportsBtn() {
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }
    public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }
    public static String PPE_Items() {
        return "//li[@id='tab_523D2817-9B4E-4B8C-B094-309D3D4C2BD4']//div[text()='PPE Items']";
    }
    public static String SaveSupportingDocuments_SaveBtn() {
        return "//div[@id='control_6448CBD2-5C8B-4CC9-83E7-9D64EC84D148']//div[text()='Save supporting documents']";
    }
    public static String supporting_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String linkbox() {
        return "//b[@original-title='Link to a document']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String validateSave(){
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String Details() {
        return "(//div[@id='control_9AA34973-FC37-4B6A-B318-634A5634EB2C']//textarea)[1]";
    }
    public static String OperateMaintenance() {
        return "//label[text()='Operate Maintenance']//..//i";
    }
    public static String PPE_Type() {
        return "(//div[@id='control_3A2ED445-A92A-4576-B300-745442E6B00E']//input)[1]";
    }
    public static String PPE_Type_Library_AddButton() {
        return "//div[@id='btnActAddNew']";
    }
    public static String SaveButton() {
        return "//div[@id='btnSave_form_0F21CE87-B806-4FDC-A24A-446A25CC4B66']";
    }
    public static String businessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String businessUnit_Checkbox(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String businessUnit_dropdown() {
        return "//div[@id='control_A5DEE1E2-018A-4718-95A1-FDE857806FBD']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String PPE_Type_Library_Module(){
        return "//label[text()='PPE Type Library']";
    }
    public static String process_flow() {
        return "//div[@id='btnProcessFlow_form_0F21CE87-B806-4FDC-A24A-446A25CC4B66']";
    }
   
}
