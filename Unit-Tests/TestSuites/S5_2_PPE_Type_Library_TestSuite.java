/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SKhumalo
 */
public class S5_2_PPE_Type_Library_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_PPE_Type_Library_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;

        //*******************************************
    }

    //S5_2_PPE_Type_Library_QA01S5_2
    @Test
    public void S5_2_PPE_Type_Library_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_PPE_Type_Library_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture PPE Type - Main Scenario
    @Test
    public void FR1_Capture_Engagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Type Library v5.2\\FR1-Capture PPE Type - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_PPE_Type_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Type Library v5.2\\FR1-Capture PPE Type - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture PPE Items - Main Scenario
    @Test
    public void FR2_Capture_PPE_Items_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Type Library v5.2\\FR2-Capture PPE Items - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR3-View Reports - Main Scenario
    @Test
    public void FR3_View_Reports_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Type Library v5.2\\FR3-View Reports - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
